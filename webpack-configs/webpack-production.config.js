const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var modules = require('./webpack.config.js');

modules = modules.map(mod => {

    mod.devtool = false;
    mod.mode = 'production';
    mod.output.path = mod.output.path.replace('output', 'dist');
    mod.plugins.push(
        new UglifyJSPlugin({
            sourceMap: true
        })
    );

    return mod;
});

module.exports = modules;
