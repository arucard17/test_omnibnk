const path = require("path");
const webpack = require("webpack");
const BundleTracker = require("webpack-bundle-tracker");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const { VueLoaderPlugin } = require("vue-loader");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const ROOTPATH = path.join(__dirname, "..", "..", "test_omnibnk", "static", "test_omnibnk");

module.exports = {
	context: ROOTPATH,
	entry: ["./js/main.js", "./scss/main.scss"],
	mode: "development",
	devtool: "inline-source-map",
	output: {
		path: path.join(ROOTPATH, "build"),
		filename: "main-[hash].js"
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ["vue-style-loader", "css-loader", "sass-loader"]
			},
			{
				test: /\.js$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				options: {
					presets: ["@babel/preset-env"],
					plugins: ["@babel/plugin-transform-runtime"]
				}
			},
			{
				test: /\.vue$/,
				use: {
					loader: "vue-loader",
					options: {
						extractCSS: process.env.NODE_ENV === "production",
						loaders: {
							sass: "vue-style-loader!css-loader!sass-loader?indentedSyntax=1",
							scss: "vue-style-loader!css-loader!sass-loader",
							less: "vue-style-loader!css-loader!less-loader"
						}
					}
				}
			},
		],
	},

	plugins: [
		new CleanWebpackPlugin(),
		new VueLoaderPlugin(),
		new BundleTracker({ filename: "./webpack-configs/stats/webpack-stats.json" }),
		new ExtractTextPlugin({ // define where to save the file
			filename: "./main-[hash].css",
			allChunks: true,
		})
	],
	resolve: {
		alias: {
			"@": path.join(ROOTPATH, "js"),
			"vue$": "vue/dist/vue.esm.js"
		},
		extensions: [".js", ".vue", ".json", ".css"]
	},
};
