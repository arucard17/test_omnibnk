from rest_framework import viewsets
from .serializers import MovieSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Movie


class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = MovieSerializer

    def get_queryset(self):
        queryset = Movie.objects.order_by('-id')
        favorite = self.request.query_params.get('favorite', None)
        if favorite is not None and favorite == 'yes':
            queryset = queryset.filter(favorite=True)
        return queryset

    @action(methods=['post'], detail=True)
    def status(self, request, pk=None):
        try:
            movie = Movie.objects.get(pk=pk)
        except Movie.DoesNotExist:
            return Response({'success': False, 'error': 'No se encontró el elemento.'}, 404)
        except Exception:
            return Response({'success': False, 'error': 'Error con el servidor, intentalo de nuevo más tarde.'}, 404)

        try:
            favorite = int(request.data['status'])
        except Exception:
            favorite = 0
        
        movie.favorite = True if favorite == 1 else False
        movie.save()

        return Response({'success': True, 'favorite': favorite, 'pk': pk, 'origin_favorite':request.data['status']})
