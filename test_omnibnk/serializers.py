from .models import Movie
from rest_framework import serializers


class MovieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Movie
        fields = ['id', 'title', 'slug', 'cover', 'favorite', 'synopsis', 'user_id']

    cover = serializers.ImageField(use_url=False, allow_empty_file=True, required=False)

    def create(self, validated_data):
        return Movie.objects.create(user_id=self.context['request'].user.id, **validated_data)
