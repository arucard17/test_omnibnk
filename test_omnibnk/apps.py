# -*- coding: utf-8 -*-
from django.apps import AppConfig


class TestOmnibnkConfig(AppConfig):
    name = 'test_omnibnk'
    verbose_name = 'Administración'
