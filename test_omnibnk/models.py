from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django.contrib.auth.models import User


class Movie(models.Model):
    title = models.CharField(_("Title"), max_length=255)
    slug = models.CharField(unique=True, blank=True, null=True, max_length=255)
    cover = models.ImageField(_("Cover"), blank=True, null=True, upload_to='movies/')
    favorite = models.BooleanField(_("Favorite"), default=False)
    synopsis = models.TextField(_("Synopsis"))

    user = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='movies',
        verbose_name=_("User"))

    def save(self, *args, **kwargs):
        if self.slug is None or (self.slug is not None and self.slug.replace(" ", "") == ''):
            self.slug = slugify(self.title)

        return super(Movie, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("Movie")
        verbose_name_plural = _("Movies")
