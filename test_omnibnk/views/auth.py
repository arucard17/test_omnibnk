# -*- coding: utf-8 -*-
import datetime

from django.http import Http404
from django.db import IntegrityError
from django.contrib.auth import authenticate
from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.exceptions import ValidationError
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from test_omnibnk.utils import validateEmpty
from django.shortcuts import render
from django.views import View


class RegisterView(View):
    template_name = 'registration/register.html'

    def get(self, request, *args, **kwargs):
        link_next = request.GET.get('next', '')
        return render(request, self.template_name, {'next': link_next})

    def post(self, request, *args, **kwargs):
        link_next = request.POST.get('next', '')

        try:
            full_name = validateEmpty(request.POST.get('full_name', None), "Debe colocar su nombre")
            email = validateEmpty(request.POST.get('email', None), "Debe colocar su dirección de correo")
            password = validateEmpty(request.POST.get('password', None), "Debe colocar la contraseña")
            password_confirmation = validateEmpty(request.POST.get('password_confirmation', None), "Debe colocar la confirmación de la contraseña")

            n = request.POST.get('next', '')

            if password != password_confirmation:
                raise ValidationError(
                    "Las contraseñas no coinciden",
                    code='invalid'
                )

        except Exception as e:
            return render(request, self.template_name, {
                'message': e.message,
                'inputs': request.POST.dict(),
                'next': link_next
            })

        try:
            user = User.objects.create_user(first_name=full_name,
                                            username=email,
                                            email=email,
                                            password=password)
            user.backend = 'apps.emailusernames.backends.EmailAuthBackend'
        except IntegrityError:
            return render(request, self.template_name, {
                'message': 'El usuario ya está siendo usado por otro usuario.',
                'inputs': request.POST.dict(),
                'next': link_next
            })
        except Exception as e:
            return render(request, self.template_name, {
                'message': 'Error al registrar el usuario, intenta de nuevo más tarde.',
                'inputs': request.POST.dict(),
                'next': link_next
            })

        user = authenticate(username=email, password=password)

        if user is not None:
            login(request, user)

            if n is not None and n != '':
                return redirect(n)
            else:
                return redirect('/')
        else:
            return render(request, self.template_name, {
                'message': 'Error al iniciar sesión. Intentelo manualmente',
                'inputs': request.POST.dict(),
                'next': link_next
            })
