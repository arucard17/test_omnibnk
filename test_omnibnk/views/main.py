from django.shortcuts import render
from django.views import View

class HomeView(View):
    template_name = 'main/home.html'
    template_name_login = 'main/home_user.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return render(request, self.template_name_login)
        else:
            return render(request, self.template_name)
