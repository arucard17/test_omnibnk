# Test Omnibank

## Dependencies

1. Use pipenv https://docs.pipenv.org/ for manage env.

## Install proccess

1. Run `pipenv install && npm install`
2. Run `npm run migrate`
3. Run `npm run createuser`

## Run server

* Run `npm start`
* Go to http://localhost:8081

## Test app

* Run `npm test`



