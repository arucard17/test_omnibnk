from rest_framework.test import APITestCase, RequestsClient
from django.contrib.auth.models import User
from .models import Movie
from .serializers import MovieSerializer


class MovieTests(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='john',
                                        email='jlennon@beatles.com',
                                        password='glass onion')
    
    def test_movie_permission(self):
        # Test permissions
        response = self.client.get('/api/movie/')
        self.assertEqual(response.status_code, 403)

    def test_can_get_movie_list(self):
        self.client.force_authenticate(self.user)

        response = self.client.get('/api/movie/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data, list(Movie.objects.order_by('-id').values()))

        Movie.objects.create(
            title='Batman', favorite=False, synopsis='I\'m Batman', user_id=self.user.id)

        response = self.client.get('/api/movie/')
        self.assertEqual(
            len(response.data), Movie.objects.order_by('-id').count())

    def test_can_delete_product(self):
        self.client.force_authenticate(self.user)

        movie = Movie.objects.create(
            title='Batman', favorite=False, synopsis='I\'m Batman', user_id=self.user.id)

        response = self.client.delete(f'/api/movie/{movie.id}/')
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Movie.objects.count(), 0)

    def test_can_client_create_movie(self):
        self.client.force_authenticate(self.user)

        response = self.client.post('/api/movie/', {
            'title': 'Batman',
            'favorite': 'no',
            'synopsis': 'I\'m Batman'
        }, format='json')

        self.assertEqual(response.status_code, 201)

        # Test updated data
        response = self.client.get(f'/api/movie/')
        for movie in response.data:
            self.assertEqual(movie['favorite'], False)
            self.client.delete(f'/api/movie/{movie["id"]}/')

        response = self.client.post('/api/movie/', {
            'title': 'Robin',
            'favorite': 'yes',
            'synopsis': 'I\'m Robin'
        }, format='json')

        self.assertEqual(response.status_code, 201)

        # Test updated data
        response = self.client.get(f'/api/movie/')
        for movie in response.data:
            self.assertEqual(movie['favorite'], True)
            self.client.delete(f'/api/movie/{movie["id"]}/')

    def test_can_client_update_movie(self):
        movie = Movie.objects.create(
            title='Batman', favorite=False, synopsis='I\'m Batman', user_id=self.user.id)

        self.client.force_authenticate(self.user)

        response = self.client.put(f'/api/movie/{movie.id}/', {
            'title': 'Teen Titans',
            'favorite': 'yes',
            'synopsis': 'I\'m Robin'
        }, format='json')

        self.assertEqual(response.status_code, 200)

        # Test updated data
        response = self.client.get(f'/api/movie/{movie.id}/')
        self.assertEqual(response.data['title'], 'Teen Titans')
        self.assertEqual(response.data['favorite'], True)

    def test_can_client_update_movie_status(self):
        movie = Movie.objects.create(
            title='Batman', favorite=False, synopsis='I\'m Batman', user_id=self.user.id)

        self.client.force_authenticate(self.user)

        response = self.client.post(f'/api/movie/{movie.id}/status/', {
            'status': 1
        }, format='json') 

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['success'], True)

        # Test updated data
        response = self.client.get(f'/api/movie/{movie.id}/')
        self.assertEqual(response.data['favorite'], True)
