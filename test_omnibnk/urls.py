from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.views import static as django_static_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework import routers

from .views import HomeView, RegisterView
from .view_sets import MovieViewSet

router = routers.DefaultRouter()
router.register(r'movie', MovieViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^movie/', HomeView.as_view()),

    # User session
    url(r'^logout', auth_views.LogoutView.as_view(), name="logout"),
    url(r'^login', auth_views.LoginView.as_view(), name="login"),
    url(r'^register/$', RegisterView.as_view(), name="user_register"),
    url(r'^api/', include(router.urls)),
]

urlpatterns = [
    url(r'^media/(?P<path>.*)$', django_static_views.serve,
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
] + staticfiles_urlpatterns() + urlpatterns
