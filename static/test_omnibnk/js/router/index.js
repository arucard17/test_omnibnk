import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

Vue.component('router-link', Vue.options.components.RouterLink);
Vue.component('router-view', Vue.options.components.RouterView);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/movie/:id/view',
      name: 'movie-view-page',
      component: require('@/components/MovieViewPage').default
    },
    {
      path: '/movie/new',
      name: 'movie-new-page',
      component: require('@/components/MovieNewPage').default
    },
    {
      path: '/movie/:id/edit',
      name: 'movie-edit-page',
      component: require('@/components/MovieEditPage').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
