FROM python:3.6

ENV PATH="/root/.local/bin:${PATH}"

COPY . /app
WORKDIR /app

RUN pip install pipenv

RUN pipenv install --deploy

CMD pipenv run python manage.py runserver 0.0.0.0:8081